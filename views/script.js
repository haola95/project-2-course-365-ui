
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB1 = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Clare Robeson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}
var vCoursesPopularTrue = [];
var vCoursesPopularFalse = [];
var vCoursesTrendingTrue = [];
var vCoursesTrendingFalse = [];

var gCoursesDB = {};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();


});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    console.log("onPageLoading");
    // gọi Api lấy danh sách môn học
    callApiGetCourse();
}


/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// hàm call api lấy danh sách môn học
function callApiGetCourse() {
    "use strict"
    // mở và gửi request đi
    $.ajax({
        url: "/devcamp-course365/courses",
        type: 'GET',
        dataType: 'json', // added data type
        success: function (CourseList) {
            // lưu dữ liệu vào gListCourse
            var vCoursePopularList = getDataCoursesPopular(CourseList);
            loadDataCoursesPopular(vCoursePopularList);
            var vCourseTrendingList = getDataCoursesTrending(CourseList);
            loadDataCoursesTrending(vCourseTrendingList);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}

// hàm lấy ra được danh sách các courses Popular
function getDataCoursesPopular(paramCoursesDB) {
    "use strict"
    var vCoursePupularList = [];
    for (var bI = 0; bI < paramCoursesDB.courses.length; bI++) {
        if (paramCoursesDB.courses[bI].isPopular) {
            vCoursePupularList.push(paramCoursesDB.courses[bI]);
        }
    }
    console.log(vCoursePupularList)
    return vCoursePupularList;
}
// hàm ra được danh sách các courses Trending
function getDataCoursesTrending(paramCoursesDB) {
    "use strict"
    var vCourseTrendingList = [];
    for (var bI = 0; bI < paramCoursesDB.courses.length; bI++) {
        if (paramCoursesDB.courses[bI].isTrending) {
            vCourseTrendingList.push(paramCoursesDB.courses[bI]);
        }
    }
    console.log(vCourseTrendingList)
    return vCourseTrendingList;
}
// hàm hiển thị 04 courses ra vùng Popular
function loadDataCoursesPopular(paramCoursesPopular) {
    for (var bI = 0; bI < paramCoursesPopular.length; bI++) {
        var bInnerHtml = `
              <div class="col-sm-3">
              <div class="card">
                <div class="text-white ">
                  <img style=" width: 100%" src=${paramCoursesPopular[bI].coverImage}  >
                </div>
                <div class="card-body">
                  <p color: rgba(5, 5, 238, 0.87)"><b>`+ paramCoursesPopular[bI].courseName + `</b></p>
                  <p > <i class="fas fa-solid fa-clock"></i> `+ paramCoursesPopular[bI].duration + ` ` + paramCoursesPopular[bI].level + `</p>
                  <p> <b>$`+ paramCoursesPopular[bI].discountPrice + `</b> <strike>$` + paramCoursesPopular[bI].price + `</strike>  </p>
                </div>
                <div class="card-footer text-left">
                  <p><img style=" clip-path: circle(18px at 50% 50%); width: 18%;" src=`+ paramCoursesPopular[bI].teacherPhoto + `>  ` + paramCoursesPopular[bI].teacherName + ` <i style="margin-left: 40px;" class="fas fa-bookmark"></i></p>
                </div>
              </div>
            </div>`
        // Append to HTML
        $('.popular-courses').append(bInnerHtml)
    }
}
// hàm hiển thị 04 courses ra vùng Trending
function loadDataCoursesTrending(paramCoursesTrending) {
    for (var bI = 0; bI < paramCoursesTrending.length; bI++) {
        var bInnerHtml = `
              <div class="col-sm-3">
              <div class="card">
                <div class="text-white ">
                  <img style=" width: 100%" src=`+ paramCoursesTrending[bI].coverImage + `>
                </div>
                <div class="card-body">
                  <p style="color: rgba(5, 5, 238, 0.87)"><b>`+ paramCoursesTrending[bI].courseName + `</b></p>
                  <p > <i class="fas fa-solid fa-clock"></i> `+ paramCoursesTrending[bI].duration + ` ` + paramCoursesTrending[bI].level + `</p>
                  <p> <b>$`+ paramCoursesTrending[bI].discountPrice + `</b> <strike>$` + paramCoursesTrending[bI].price + `</strike>  </p>
                </div>
                <div class="card-footer text-left">
                  <p><img style=" clip-path: circle(18px at 50% 50%); width: 18%;" src=`+ paramCoursesTrending[bI].teacherPhoto + `>  ` + paramCoursesTrending[bI].teacherName + ` <i style="margin-left: 40px;" class="fas fa-bookmark"></i></p>
                </div>
              </div>
            </div>`
        // Append to HTML
        $('.trending-courses').append(bInnerHtml)
    }
}


