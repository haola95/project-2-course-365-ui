// Import bo thu vien express
const express = require('express');

const path = require("path")

// Import Course Middleware
const { printCourseURLMiddleware } = require('../middlewares/courseMiddlewares.js');

const router = express.Router();

// Khai báo middleware đọc Json
router.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
router.use(express.urlencoded ({
    extended: true
}))

// Import drink Controller
const { createCourse, getAllCourse, getCourseById, updateCourseById, deleteCourseById } = require("../controllers/courseController.js")

router.use(express.static(__dirname + "../../../views"))

router.get("/", printCourseURLMiddleware, (request, response) => {
    console.log(__dirname)
    response.sendFile(path.join(__dirname+'../../../views/index.html'));
  
});

router.get("/devcamp-course365/courses", getAllCourse);

router.post("/devcamp-course365/courses", createCourse);

router.get("/devcamp-course365/courses/:courseId", getCourseById);

router.put("/devcamp-course365/courses/:courseId", updateCourseById);

router.delete("/devcamp-course365/courses/:courseId", deleteCourseById);
// Export dữ liệu 1 module
module.exports = router;