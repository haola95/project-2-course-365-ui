// Import drink model vào controller
const courseModel = require("../models/courseModel.js");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createCourse = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!bodyRequest.courseCode) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course Code is required"
        })
    }

    if (!bodyRequest.courseName) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course Name is required"
        })
    }

    if (!bodyRequest.price) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Price is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.price))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Price is not valid"
        })
    }

    if (!bodyRequest.discountPrice) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Discount Price is required"
        })
    }

    if (!(Number.isInteger(bodyRequest.discountPrice))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Discount Price is not valid"
        })
    }

    if (!bodyRequest.duration) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Druration is required"
        })
    }

    if (!bodyRequest.level) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Level is required"
        })
    }

    if (!bodyRequest.coverImage) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Cover Image is required"
        })
    }

    if (!bodyRequest.teacherName) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Teacher Name is required"
        })
    }

    if (!bodyRequest.teacherPhoto) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Teacher Photo is required"
        })
    }

    if (!bodyRequest.isPopular) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Is Popular is required"
        })
    }

    if (!bodyRequest.isTrending) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Is Trending is required"
        })
    }



    // B3: Thao tác với cơ sở dữ liệu
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: bodyRequest.courseCode,
        courseName: bodyRequest.courseName,
        price: bodyRequest.price,
        discountPrice: bodyRequest.discountPrice,
        duration: bodyRequest.duration,
        level: bodyRequest.level,
        coverImage: bodyRequest.coverImage,
        teacherName: bodyRequest.teacherName,
        teacherPhoto: bodyRequest.teacherPhoto,
        isPopular: bodyRequest.isPopular,
        isTrending: bodyRequest.isTrending
    }

    courseModel.create(createCourse, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Created Course Success ",
                course: data
            })
        }

    })
}

const getAllCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Thao tác với cơ sở dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                description: "This DB includes all courses in system",
                courses: data
            })
        }
    })
}

const getCourseById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(courseId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get Course By Id: " + courseId,
                course: data
            })
        }
    })


}

const updateCourseById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId
    let bodyRequest = request.body;

    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(courseId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Drink ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    let courseUpdate = {
        courseCode: bodyRequest.courseCode,
        courseName: bodyRequest.courseName,
        price: bodyRequest.price,
        discountPrice: bodyRequest.discountPrice,
        duration: bodyRequest.duration,
        level: bodyRequest.level,
        coverImage: bodyRequest.coverImage,
        teacherName: bodyRequest.teacherName,
        teacherPhoto: bodyRequest.teacherPhoto,
        isPopular: bodyRequest.isPopular,
        isTrending: bodyRequest.isTrending
    }

    courseModel.findByIdAndUpdate(courseId, courseUpdate, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update Course success",
                courses: data
            })
        }
    })
}

const deleteCourseById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    let courseId = request.params.courseId
    // B2: Validate dữ liệu
    if (!(mongoose.Types.ObjectId.isValid(courseId))) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Course ID is not valid"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Delete Course Success ",
                course: data
            })
        }
    })
}
// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    createCourse: createCourse,
    getAllCourse: getAllCourse,
    getCourseById: getCourseById,
    updateCourseById: updateCourseById,
    deleteCourseById: deleteCourseById
}